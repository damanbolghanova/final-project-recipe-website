function validate(){
  var email = document.getElementById("email").value;
  var fname = document.getElementById("fname");
  var lname = document.getElementById("lname");
  var message = document.getElementById("message");

  if(fname.value == ""){
    alert("Please, Enter Your First Name");
    return false;
  }

  if(lname.value == ""){
    alert("Please, Enter Your Last Name");
    return false;
  }

  if(email.value == "" || email.indexOf("@") == -1){
    alert("Please Enter Valid Email");
    return false;
  }

  if(message.value == ""){
    alert("Please, Enter Your Comment");
    return false;
  }
    alert("Message Submitted Successfully!\n Thank You For Your Comment!");
}

function subscribe(){
  var emailSub = document.getElementById("emailSub").value;

  if(emailSub.value == "" || emailSub.indexOf("@") == -1){
    alert("Please Enter Valid Email");
    return false;
  }

  alert("You Successfully Subscribed To Our NewsLetters!\n Thank You!");
}