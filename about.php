<?php require 'php/includes/header.php'; ?>
<title>About Us</title>
<link href="css/about.css" rel="stylesheet">
<!--WhatFind-->
        <section id="whatFind">
          <div class="container">
              <div class="row">
                  <div class="col-md-5">
                      <div class="Image">
                          <img src="https://www.google.com/url?sa=i&url=https%3A%2F%2Fwebstockreview.net%2Fexplore%2Fcooking-clipart-kitchen-team%2F&psig=AOvVaw3Zac5EAcMgl86UnDwWy16m&ust=1589126287187000&source=images&cd=vfe&ved=0CAIQjRxqFwoTCNilpvOSp-kCFQAAAAAdAAAAABAg" class="rounded" alt="Picture1" style="position: absolute; top: 10px; left: 0px;" width="350" height="300">
                      </div>
                  </div>

                  <div class="col-md-7">
                    <h2>What We Do?</h2>
                    <div class="whatFind-content">
                        <p style="text-indent: 35px;">All of these recipes are made, tested and adored by me. I love sharing my hobby of cooking with others.
                          I hope that you wil love these recipes as much as I do. You don’t need to be a master chef to make these, in fact,
                          just a littli bit of a cooking experience is enough! There are several simple recipes that you can start with.<br></p>
                        <p style="text-indent: 35px;">As I am just starting blogging and sharing my recipes, there are only several ones. So, don't be confused
                        by the number of recipes here. As time goes on, there will be more recipes. Good Luck!</p>
                    </div>
                </div>
              </div>
          </div>
        </section>

         <!--Our Contacts-->
        <section id="ourContacts">
            <h3>Where Can You Find Us?</h3>
            <div class="container text-center">
              <h5>1.Find Us On Social Media</h5>
          </div>
          <div class="socialMedia-Icons">
              <a href="https://web.facebook.com/dariya.amanbolganova.587" target="_blank"><img src="https://www.stcatherineparish.net/media/1/Logos/facebook.png"></a>
              <a href="https://www.instagram.com/" target="_blank"><img src="https://www.fertility-experiences.com/wp-content/uploads/2019/11/instagram-scaled.png"></a>
              <a href="https://www.youtube.com/channel/UCJFp8uSYCjXOMnkUyb3CQ3Q" target="_blank"><img src="https://logosmarcas.com/wp-content/uploads/2018/06/Youtube-Emblema.png"></a>
              <a href="https://web.whatsapp.com/" target="_blank"><img src="https://2.bp.blogspot.com/-N2baoXbJTT4/XE7NmKTFPNI/AAAAAAAAHV4/wPigcVbi9Xs46i3Ra41y9XSyll7QAe0pwCK4BGAYYCw/s1600/Icon-WhatsApp.png"></a>
          </div>
          <div class="container text-center">
            <h5 style="margin-top: 50px;">2.Visit Us</h5>
            <p><iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d5011.956586784308!2d71.41406578604455!3d51.09040810652384!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x424585a605525605%3A0x4dff4a1973f7567e!2sAstana%20IT%20University!5e0!3m2!1sru!2skz!4v1584913913601!5m2!1sru!2skz" width="800" height="450" frameborder="0" style="border:0;" allowfullscreen="" aria-hidden="false" tabindex="0"></iframe></p>
        </div>
          </div>
      </section>

<?php require 'php/includes/footer.php'; ?>
