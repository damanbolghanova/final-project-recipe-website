<?php
require 'php/includes/database.php'; ?>
<title>Breakfast</title>
<?php
$sql = "SELECT * FROM breakfasts_tb"; //RETRIEVE SMTH FROM DATABASE
$res = mysqli_query($conn, $sql); //execute the query
if ($res === false) {
  echo mysqli_error($conn);
}else{
  $recipes = mysqli_fetch_all($res, MYSQLI_ASSOC); //retrieves * from DATABASE
  //MYSQLI_ASSOC is for in what structure it will save the result: in this example
  // it will save it as a associative array take field as their key values
}
require 'php/includes/header.php';
?>

<title>Breakfast</title>
<link href="css/breakfast.css" rel="stylesheet">
        <h1 style="padding-top: 20px;">All happiness depends on a leisuraly breakfast!</h1>
        <h1 style="padding-top: 20px;"><b>Breakfast</b></h1>

        <div class="container">
          <?php foreach ($recipes as $recipe): ?>
            <div class="Item1">
              <img src="<?php echo $recipe['recipe_img']; ?>"><br>
              <h2><a href="recipeBreakfast.php?id=<?=$recipe['id'];?>"><?=$recipe['recipe_name'];?></a></h2>
            </div>
          <?php endforeach; ?>
            <?php
            if (isset($_SESSION['user'])){
                if ($_SESSION['user']['username']=='admin'){
                    echo "<a href='php/add/addBreakfasts.php'>Add New Recipe</a>";
                }
            }
            ?>
        </div>