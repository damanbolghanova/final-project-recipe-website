<?php require 'php/includes/header.php'; ?>
<title>Contact</title>
<link href="css/contact.css" rel="stylesheet">
        <!--Conatact-->
        <div class="container">
          <div class="row">
            <div class="col-md-6">
              <h2 style="text-align: center;">Contact</h2>
              <p style="text-align: center;">Please get in touch with me if you have any questions or suggestions about recipes and working with me!<br></p>

              <section class="login-form">
                <form>
                    <div class="box">
                      <div class="form-fields">
                        <div class="input-box">
                            <input type="text" required placeholder="Enter Your Email" class="form-control" id="email">
                            <span><img src="https://cdn2.iconfinder.com/data/icons/multimedia-outline-ve2/32/envelope_email_mail_message_letter-512.png"></span>
                        </div>

                        <div class="input-box">
                            <input type="name" required placeholder="Enter your First Name" class="form-control" id="fname">
                            <span><img src="https://i.dlpng.com/static/png/6456209_thumb.png"></span>
                        </div>

                        <div class="input-box">
                          <input type="name" required placeholder="Enter your Last Name" class="form-control" id="lname">
                          <span><img src="https://i.dlpng.com/static/png/6456209_thumb.png"></span>
                      </div>

                        <div class="input-box">
                            <label><input type="radio" name="radiobutton" value="Val1">Male</label>
                            <label><input type="radio" name="radiobutton" value="Val1">Female</label>
                        </div>

                        <div class="input-box">
                          <textarea rows="5" cols="100" name="comment" class="form-control" id="message" required placeholder="Enter Your Comment Here"></textarea>
                          <span><img src="https://cdn.iconscout.com/icon/free/png-256/chat-message-write-note-pen-type-pencile-6-14761.png"></span>
                      </div>

                        <div class="button-box">
                            <button type="submit" onclick="validate()">Submit</button>
                            <button type="reset">Reset</button>
                        </div>
                      </div>
                      <div class="social-links">
                          <p>or Send Me Message on My Socials:</p>
                          <div class="links-box">
                            <a href="https://web.facebook.com/dariya.amanbolganova.587" class="facebook" target="_blank">Facebook</a>
                            <a href="https://vk.com/damanbolganova" class="google" target="_blank" >VK</a>
                            <a href="https://twitter.com/home" class="twitter" target="_blank">Twitter</a>
                          </div>
                      </div>
                    </div>
                </form>
            </section>
            </div>

            <div class="col-md-6">
              <div class="images">
                <img src="https://images.squarespace-cdn.com/content/v1/5280c154e4b05645e635cf76/1510623075171-YSO8RPB2QR8MG7393Z2T/ke17ZwdGBToddI8pDm48kDNmyC5FM8GiR4og3MgQa1x7gQa3H78H3Y0txjaiv_0fDoOvxcdMmMKkDsyUqMSsMWxHk725yiiHCCLfrh8O1z4YTzHvnKhyp6Da-NYroOW3ZGjoBKy3azqku80C789l0tb-hnCqoepq4X8c1traqO_JWRA3fEpTuA5K9wzrbyCcSebwWA2O1ajdB4h3_NxcMA/Blueberry+Basil+Coconut+Milk+Popsicles.+A+refreshing+dessert+%7C+This+Healthy+Table?format=1500w" class="image1"></img>
                <img src="https://images.squarespace-cdn.com/content/v1/5280c154e4b05645e635cf76/1511647407485-RZOTBN6L3F9WZ52J094Q/ke17ZwdGBToddI8pDm48kDNmyC5FM8GiR4og3MgQa1x7gQa3H78H3Y0txjaiv_0fDoOvxcdMmMKkDsyUqMSsMWxHk725yiiHCCLfrh8O1z4YTzHvnKhyp6Da-NYroOW3ZGjoBKy3azqku80C789l0tb-hnCqoepq4X8c1traqO_JWRA3fEpTuA5K9wzrbyCcSebwWA2O1ajdB4h3_NxcMA/Leek+%26+Pea+Fried+Rice.+A+vegetarian+main+dish+%7C+This+Healthy+Table?format=1500w" class="image2"></img>
                <img src="https://images.squarespace-cdn.com/content/v1/5280c154e4b05645e635cf76/1538958162411-OQ5PAU4PGPL9L11WZUF2/ke17ZwdGBToddI8pDm48kDNmyC5FM8GiR4og3MgQa1x7gQa3H78H3Y0txjaiv_0fDoOvxcdMmMKkDsyUqMSsMWxHk725yiiHCCLfrh8O1z4YTzHvnKhyp6Da-NYroOW3ZGjoBKy3azqku80C789l0tb-hnCqoepq4X8c1traqO_JWRA3fEpTuA5K9wzrbyCcSebwWA2O1ajdB4h3_NxcMA/Spinach+%26+Feta+Stuffed+Salmon+%7C+a+delicious%2C+weeknight+meal+that%27s+ready+in+20+minutes+%23salmon+%23recipe?format=1500w" class="image3"></img>
                <img src="https://images.squarespace-cdn.com/content/5280c154e4b05645e635cf76/1524504975153-SJGESHQX0S4A7X0D4065/strawberry+tahini+shortcake+%283+of+4%29.jpg?format=300w&content-type=image%2Fjpeg" class="image4"></img>
                <img src="https://static1.squarespace.com/static/5280c154e4b05645e635cf76/545f22b6e4b054a6f864180d/56334a10e4b05e8dfbf38f5f/1560870162274/Coconut%2BAlmond%2BMilk%2BPancakes%2Bwith%2BBlueberry%2BCompote%2B-%2BShiny%2BHappy%2BBright.jpeg?format=500w" class="image5"></img>
                <img src="https://images.squarespace-cdn.com/content/v1/5280c154e4b05645e635cf76/1530653724065-YN9BJ24LYKVAYM3ET4CA/ke17ZwdGBToddI8pDm48kDNmyC5FM8GiR4og3MgQa1x7gQa3H78H3Y0txjaiv_0fDoOvxcdMmMKkDsyUqMSsMWxHk725yiiHCCLfrh8O1z4YTzHvnKhyp6Da-NYroOW3ZGjoBKy3azqku80C789l0tb-hnCqoepq4X8c1traqO_JWRA3fEpTuA5K9wzrbyCcSebwWA2O1ajdB4h3_NxcMA/Vegan+Chocolate+Milkshake+-+a+super+easy+%26+delicious%2C+dairy-free%2C+refined+sugar+free+recipe+for+a+raw+cacao+shake.?format=1500w" class="image6"></img>
              </div>
            </div>
          </div>
        </div>

<?php require 'php/includes/footer.php'; ?>
