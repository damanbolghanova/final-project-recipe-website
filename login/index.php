<?php require '../php/includes/header.php';

//session_start();
if (!isset($_SESSION)) {
header("Location: auth.php");
}
?>
<script>

</script>
<title>Welcome!</title>
<link href="../css/style.css" rel="stylesheet">

        <!--Banner-->
        <section id="banner">
            <div class="container-fluid">
                <div class="row">
                    <div class="col-md-12">
                        <h2 style="text-align: center; margin-top: 30px; color: white">
                            <?php
                            if (isset($_SESSION['user'])) {
                                echo "Welcome ". $_SESSION['user']['username'];
                            }
                            ?>
                        </h2>
                        <div class="content">
                            <div class="content__container">
                                <ul class="content__container__list">
                                    <li class="content__container__list__item">Eat Healthy!</li><br>
                                    <li class="content__container__list__item">Live Long!</li><br>
                                    <li class="content__container__list__item">Live Strong!</li><br>
                                  </ul>
                            </div>
                        </div>

                    </div>
                </div>
            </div>
        </section>


        <!--WhatFind-->
        <section id="whatFind">
            <div class="container">
                <div class="row">
                    <div class="col-md-6">
                        <h2>What This Website Is About?</h2>
                        <div class="whatFind-content">
                            <p>Hi! I'm Dariya. Here you can find various recipes whichever you want! I swear all meals described
                            here are tasty and easy to prepare.<br> Trust me :) <br></p>
                        <p>For your convinience, recipes are devided into groups: Breakfasts, Snacks, Dinner and Lunch. In this website all
                    ingridients and cooking processes are described. Also, some photos are provided<br></p>
               <p> Hope that you'll enjoy it. Wish you goood luck!<br></p>
           <p> Don't be shy to text me, if you have any questons or suggestions. You can do it in <a href="contact.html">Contacts</a> page :)</p>
                        </div>
                    </div>
                    <div class="col-md-6">
                        <div class="Image">
                            <img src="https://images.squarespace-cdn.com/content/v1/5280c154e4b05645e635cf76/1451866460784-0WZPOM5WE709IB171LU5/ke17ZwdGBToddI8pDm48kFQQgP34qnCpeHaeAOzTt7pZw-zPPgdn4jUwVcJE1ZvWQUxwkmyExglNqGp0IvTJZamWLI2zvYWH8K3-s_4yszcp2ryTI0HqTOaaUohrI8PICHnXC1b9smDvYLPdL-DS7U1pkhCtl83kemXd5r3C5ngKMshLAGzx4R3EDFOm1kBS/Almond+Milk+%26+Vanilla+Steel+Cut+Oatmeal+with+a+Peach+Compote+%7C+Shiny+Happy+Bright?format=1000w" alt="Picture1" style="position: absolute; top: 10px; left: 0px; border: 3px solid #ffa177; border-radius: 35px;" width="350" height="300">
                            <img src="https://images.squarespace-cdn.com/content/v1/5280c154e4b05645e635cf76/1511647464181-UVE887EKZ0YJ1S7CCEPQ/ke17ZwdGBToddI8pDm48kDNmyC5FM8GiR4og3MgQa1x7gQa3H78H3Y0txjaiv_0fDoOvxcdMmMKkDsyUqMSsMWxHk725yiiHCCLfrh8O1z4YTzHvnKhyp6Da-NYroOW3ZGjoBKy3azqku80C789l0tb-hnCqoepq4X8c1traqO_JWRA3fEpTuA5K9wzrbyCcSebwWA2O1ajdB4h3_NxcMA/Leek+%26+Pea+Fried+Rice.+A+vegetarian+main+dish+%7C+This+Healthy+Table?format=1500w" alt="Picture2" style="z-index: 1; position: absolute; top: 110px; left: 250px; border: 3px solid #ffa177; border-radius: 35px;" width="300" height="280">
                        </div>
                    </div>
                </div>
            </div>
        </section>


        <!--Recipes-->
        <section id="recipes">
            <div class="container text-center">
                <h1 class="title">RECIPES</h1>
                <div class="row text-center">
                    <div class="col-md-4 recipes">
                        <a href="../breakfast.php"><img class="recipes-img" src="https://images.fitnessmagazine.mdpcdn.com/sites/fitnessmagazine.com/files/healthiest-breakfast-foods_0.jpg"></a>
                        <h4>Breakfast</h4>
                        <p>Here you can find easy and healthy <b>BREAKFAST</b> recipes</p>
                        <button type="button" class="btn btn-primary" onclick="parent.location = 'breakfast.php'">See More</button>
                    </div>
                    <div class="col-md-4 recipes">
                        <a href="../lunch.php"><img class="recipes-img" src="https://www.femalefirst.co.uk/image-library/square/1000/n/nandos-chicken-butterfly-with-saucy-spinach-and-grains-n-greens.jpg"></a>
                        <h4>Lunch</h4>
                        <p>Here you can find easy and healthy <b>LUNCH</b> recipes</p>
                        <button type="button" class="btn btn-primary" onclick="parent.location = 'lunch.php'">See More</button>
                    </div>
                    <div class="col-md-4 recipes">
                        <a href="../dinner.php"><img class="recipes-img" src="https://imagesvc.meredithcorp.io/v3/mm/image?url=https%3A%2F%2Fimg1.southernliving.timeinc.net%2Fsites%2Fdefault%2Ffiles%2Fstyles%2Fmedium_2x%2Fpublic%2Fimage%2F2017%2F01%2Fmain%2Fdinners-2504601_month_17187.jpg%3Fitok%3Dsbct6EBG" height="300"></a>
                        <h4>Dinner</h4>
                        <p>Here you can find easy and healthy <b>DINNER</b> recipes</p>
                        <button type="button" class="btn btn-primary " onclick="parent.location = 'dinner.php'">See More</button>
                    </div>
                </div>
            </div>
        </section>


        <!--Social Media-->
        <section id="socialMedia">
            <div class="container text-center">
                <h3>Find Us On Social Media</h3>
            </div>
            <div class="socialMedia-Icons">
                <a href="https://web.facebook.com/dariya.amanbolganova.587" target="_blank"><img src="https://upload.wikimedia.org/wikipedia/commons/thumb/0/05/Facebook_Logo_%282019%29.png/1024px-Facebook_Logo_%282019%29.png"></a>
                <a href="https://www.instagram.com/" target="_blank"><img src="https://www.fertility-experiences.com/wp-content/uploads/2019/11/instagram-scaled.png"></a>
                <a href="https://www.youtube.com/channel/UCJFp8uSYCjXOMnkUyb3CQ3Q" target="_blank"><img src="https://www.freepnglogos.com/uploads/youtube-logo-png-hd-1.png"></a>
                <a href="https://web.whatsapp.com/" target="_blank"><img src="https://2.bp.blogspot.com/-N2baoXbJTT4/XE7NmKTFPNI/AAAAAAAAHV4/wPigcVbi9Xs46i3Ra41y9XSyll7QAe0pwCK4BGAYYCw/s1600/Icon-WhatsApp.png"></a>
            </div>
        </section>
<?php require 'C:\xampp\htdocs\finalWeb\php\includes\footer.php'; ?>
