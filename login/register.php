<?php
/*session_start();
if ($_SESSION['user']){
    header('Location: ../index.php');
}*/
?>

<!DOCTYPE html>
<html >
<head >
    <meta charset="UTF-8">
    <title>Registration</title>
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script>
    <script src="reguser.js"></script>
    <link rel="stylesheet" href="style.css">
    <style>
        .red{
            color: red;
        }
        .green{
            color: #4CAF50;
        }
    </style>
</head>
<body>
<section class="login-form" style="background-image: url('img/back.png');">
    <form id="rff" class="" action="adduser.php" method="post">
        <div class="box">
            <div class="img" style="margin-left: 143px">
                <img src="img/boy.png" alt="user">
                <img src="img/girl.png" alt="user">
            </div>
            <div class="heading">
                <h4 style="color: #FFA177; margin-left: 142px">Registration</h4>
            </div>
            <div class="form-fields">
                <div class="input-box">
                    <input id="ruser" required="required" type="text" name="ruser" placeholder="username" class="form-control">
                    <span><img src="img/user.png"></span>
                </div>

                <div class="input-box">
                    <input required type="fname" name="fname" id="fname" placeholder="First Name" class="form-control">
                    <span><img src="img/user.png"></span>
                </div>

                <div class="input-box">
                    <input required type="lname" name="lname" id="lname" placeholder="Last Name" class="form-control">
                    <span><img src="img/user.png"></span>
                </div>

                <div class="input-box">
                    <input id="rpass" required="required" type="password" name="rpass" placeholder="password" class="form-control">
                    <span><img src="img/password.png"></span>
                </div>

                <div class="button-box">
                    <input value="SignUp" type="submit" id="registr" name="registr">
                </div>
                <p >
                    Already have an account? <a href="auth.php">Register Here!</a>
                </p>
                <p id="notification"></p>
            </div>
        </div>
    </form>
</section>


</body>
</html>
