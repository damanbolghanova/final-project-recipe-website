<?php
require 'C:\xampp\htdocs\finalWeb\php\includes\database.php';
require 'C:\xampp\htdocs\finalWeb\php\includes\getRecipeDinner.php';

$recipes = getRecipe($conn, $_GET['id']);

if ($_SERVER['REQUEST_METHOD'] == 'POST') {
  $id = $_GET['id'];
  $sql = "DELETE FROM dinners_tb
    WHERE id = ?";
  $stmt = mysqli_prepare($conn, $sql);

  if ($stmt === false) {
    echo mysqli_error($conn);
  }else{
    mysqli_stmt_bind_param($stmt, "i", $id);

    if (mysqli_stmt_execute($stmt)) {
      header("Location: http://localhost/finalWeb/dinner.php ");
      exit;

    }else{
      echo mysqli_stmt_error($stmt);
    }
  }
}

 ?>
