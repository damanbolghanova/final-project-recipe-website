<br><br><br><br><br>
<section id="footer">
 <div class="container">
     <div class="row">
         <div class="col-md-4 footer-box">
             <img src="logo.png" width="200" height="150">
         </div>

         <div class="col-md-4 footer-box">
           <p><b>Contact Us</b></p>
           <p><img src="https://static.tildacdn.com/tild6661-3261-4366-b737-613662323136/place-localizer.png" width="20" height="20"> Nur-Sultan city, Kazakhstan</p>
           <p><img src="https://davidpou.net/wp-content/uploads/2017/08/Ic_phone_48px.png" width="20" height="20"> +7 707 316 08 09</p>
           <p><img src="https://cdn2.iconfinder.com/data/icons/multimedia-outline-ve2/32/envelope_email_mail_message_letter-512.png" width="20" height="20"> damanbolghanova@gmail.com</p>
       </div>

         <div class="col-md-4 footer-box">
             <p style="text-transform: uppercase;"><b>Subscribe Newsletter</b></p>
             <input type="email" class="form-control" id="emailSub" required placeholder=" Enter Your Email">
             <button type="button" class="btn btn-primary" onclick="subscribe()">Subscribe</button>
         </div>
     </div>
 </div>
</section>
</body>
</html>
