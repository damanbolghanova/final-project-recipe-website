<?php
session_start();
?>

<!DOCTYPE html>
<head>
    <meta charset="utf-8">
    <meta name="viewpoint" content="width=device-width, initial-scale=1">
    <link href="https://fonts.googleapis.com/css?family=Dancing+Script&display=swap" rel="stylesheet">
    <link href="https://stackpath.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css" rel="stylesheet">
    <link href="https://fonts.googleapis.com/css?family=Kaushan+Script&display=swap" rel="stylesheet">
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/css/bootstrap.min.css">
    <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/js/bootstrap.min.js"></script>
    <script src="https://code.jquery.com/jquery-3.4.1.slim.min.js"></script>
    <script src="https://cdn.jsdelivr.net/npm/popper.js@1.16.0/dist/umd/popper.min.js"></script>
</head>
<html>
    <body>
        <!--Navigation-->
        <section id="nav-bar">
            <nav class="navbar navbar-expand-lg navbar-light">
                <a class="navbar-brand" href="index.html"><img src="logo.png" width="100" height="60"></a>
                <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarNav" aria-controls="navbarNav" aria-expanded="false" aria-label="Toggle navigation">
                  <span class="navbar-toggler-icon"></span>
                </button>
                <div class="collapse navbar-collapse" id="navbarNav">
                  <ul class="navbar-nav ml-auto">
                    <li class="nav-item">
                      <a class="nav-link" href="login/index.php">Home</a>
                    </li>
                    <li class="nav-item active">
                      <a class="nav-link" href="recipes.php">Recipes</a>
                    </li>
                    <li class="nav-item">
                      <a class="nav-link" href="contact.php">Contacts</a>
                    </li>
                    <li class="nav-item">
                      <a class="nav-link" href="about.php">About Us</a>
                    </li>
                      <?php if (isset($_SESSION['user'])):?>
                     <li class="nav-item">
                      <a class="nav-link" href="logout.php">Logout</a>
                    </li>
                      <?php endif; ?>
                      <?php if (!isset($_SESSION['user'])): ?>
                          <li class="nav-item">
                              <a class="nav-link" href="auth.php">Login</a>
                          </li>
                          <?php endif; ?>
                  </ul>
                </div>
              </nav>
        </section>
