<?php
    session_start();
  require 'php/includes/database.php';
  require 'php/includes/header.php';
  require 'php/includes/getRecipeBreakfast.php';

/*
function createCommentRow($data){
    return '
        <div class="comment">
                <div class="user">'.$data['username'].' <span class="time">'.$data['createdOn'].'</span></div>
                <div class="userComment">'.$data['comment'].'</div>
        </div>
    ';
    return;
}

if (isset($_POST['getAllComments'])){
    $start = $conn->real_escape_string($_POST['start']);

    $response = "";
    $sql = $conn->query("SELECT comments.id, users.username, comments.comment, DATE_FORMAT(comments.createdOn, '%Y-%m-%d') AS createdOn FROM comments INNER JOIN users ON comments.user_id = users.user_id ORDER BY comments.id DESC LIMIT $start, 20");
    while ($data = $sql->fetch_assoc())
        $response .= createCommentRow($data);

    exit($response);
}


    if (isset($_POST['addComment'])) {
        $comment = $conn->real_escape_string($_POST['comment']);

        $conn->query("INSERT INTO comments (user_id, comment, createdOn) VALUES ('".$_SESSION['userID']."','$comment',NOW())");
        $sql = $conn->query("SELECT comments.id, users.username, DATE_FORMAT(comments.createdOn, '%Y-%m-%d') AS createdOn FROM comments INNER JOIN users ON comments.user_id = users.user_id ORDER BY comments.user_id DESC LIMIT 1");
        $data = $sql->fetch_assoc();
        exit(createCommentRow($data));
    }

    $sqlNumComments = $conn->query("SELECT id from comments");
    $numComments = $sqlNumComments->num_rows;
*/

  $recipes = getRecipe($conn,$_GET['id']);

  if($recipes===null): ?>
    <h2>No recipe has been found</h2>
    <?php else: ?>
      <style type="text/css">
          .comment {
              margin-bottom: 20px;
          }
          .time {
              color: gray;
          }
          .userComment {
              color: #000;
          }
      </style>
    <link href="css/style.css" rel="stylesheet">
        <!--Recipe-->
        <h1 style="padding-top: 30px; text-align: center; color: #ffa177; padding-bottom: 30px;"><?=$recipes['recipe_name']?></h1>
        <div class="container">
          <div class="row">
            <div class="col-md-6">
              <h2>I N G R I D I E N T S</h2>
              <ul class="circle">
                <li><?=$recipes['ingridients']?></p></li>
              </ul>
              <h2>I N S T R U C T I O N S</h2>
              <ul class="1">
                <li><?=$recipes['instructions']?></li><br>
              </ul>
                <?php
                if (isset($_SESSION['user'])){
                    if ($_SESSION['user']['username']=='admin'){
                        echo "<a href='php/edit/editBreakfast.php?id=".$recipes['id']."'>Edit the Recipe</a>
              <form action='php/delete/deleteBreakfast.php?id=".$recipes['id']."' method='post'>
                <button>DELETE</button>
              </form>";
                    }/*
                        else{
                        echo "
                            <textarea class='form-control' id='mainComment' placeholder='Add Public Comment' cols='30' rows='2'></textarea><br>
                            <button style='float:right' class='btn-primary btn' id='addComment'>Add Comment</button>

                        <h2><b id='numComments'><?php echo $numComments ?> Comments</b></h2>
                        <div class='userComments'>

                        </div>

                        ";
                    }*/
                }
                ?>

            </div>


            <div class="col-md-6">
              <div class="img">
                <img src="<?php echo $recipes['recipe_img']; ?>"height="700" width="700" align="center">
              </div>

               <!--Marking-->
              <div class="marking">
                <h2>Do you like it?</h2>
                <input type="range" min="1" max="100" value="10" class="slider" id="myMark">
                <p>Mark: <span id="qwerty"></span></p>
                <button type="button" class="btn btn-primary"  onclick="alert('You evaluated this recipe: ' + output.innerHTML + '\n Thank You!')" type="number">Evaluate</button>
              </div>
              <script>
                var slider = document.getElementById("myMark");
                var output = document.getElementById("qwerty");
                output.innerHTML = slider.value;

                slider.oninput = function() {
                  output.innerHTML = this.value;
                }
              </script>
                <!--<script src="http://code.jquery.com/jquery-3.4.1.min.js"></script>
                <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.7/umd/popper.min.js"</script>
                <script type="text/javascript">
                var max = <?php /* echo $numComments*/?>;
                    $(document).ready(function () {
                        $("#addComment").on('click', function () {
                            let comment = $("#mainComment").val();

                            if (comment.length > 5) {
                                $.ajax({
                                    url: 'recipeBreakfast.php',
                                    method: 'POST',
                                    dataType: 'text',
                                    data: {
                                        addComment: 1,
                                        comment: comment
                                    }, success: function (response) {
                                        max++;
                                        $("#numComments").text(max + " Comments");
                                        $(".userComments").prepend(response);
                                    }
                                });
                            } else
                                alert('Please Check Your Inputs');
                        });
                        getAllComments(0, max);
                    });

                function getAllComments(start, max) {
                    if (start > max){
                        return;
                    }
                    $.ajax({
                        url: 'recipeBreakfast.php',
                        method: 'POST',
                        dataType: 'text',
                        data: {
                            getAllComments: 1,
                            start : start
                        }, success: function (response) {
                            $(".userComments").append(response);
                            getAllComments((start+20), max);
                        }
                    });
                }
                </script>-->
            </div>
          </div>
        </div>

      <?php endif;
      require 'php/includes/footer.php'; ?>