<?php
  require 'php/includes/database.php';
  require 'php/includes/header.php';
  require 'php/includes/getRecipeSnack.php';
  $recipes = getRecipe($conn,$_GET['id']);

  if($recipes===null): ?>
    <h2>No recipe has been found</h2>
<?php else: ?>
  <link href="css/style.css" rel="stylesheet">
        <!--Recipe-->
        <h1 style="padding-top: 30px; text-align: center; color: #ffa177; padding-bottom: 30px;"><?=$recipes['recipe_name']?></h1>
        <div class="container">
          <div class="row">
            <div class="col-md-6">
              <h2>I N G R I D I E N T S</h2>
              <ul class="circle">
                <li><?=$recipes['ingridients']?></p></li>
              </ul>
              <h2>I N S T R U C T I O N S</h2>
              <ul class="1">
                <li><?=$recipes['instructions']?></li><br>
              </ul>
              <a href="php/edit/editSnack.php?id=<?=$recipes['id']?>">Edit the Recipe</a>
              <form action="php/delete/deleteSnack.php?id=<?=$recipes['id']?>" method="post">
                <button>DELETE</button>
              </form>
            </div>


            <div class="col-md-6">
              <div class="img">
                <img src="<?php echo $recipes['recipe_img']; ?>"height="700" width="700" align="center">
              </div>

               <!--Marking-->
              <div class="marking">
                <h2>Do you like it?</h2>
                <input type="range" min="1" max="100" value="10" class="slider" id="myMark">
                <p>Mark: <span id="qwerty"></span></p>
                <button type="button" class="btn btn-primary"  onclick="alert('You evaluated this recipe: ' + output.innerHTML + '\n Thank You!')" type="number">Evaluate</button>
              </div>
              <script>
                var slider = document.getElementById("myMark");
                var output = document.getElementById("qwerty");
                output.innerHTML = slider.value;

                slider.oninput = function() {
                  output.innerHTML = this.value;
                }
              </script>

            </div>
          </div>
        </div>
      <?php endif;

      require 'php/includes/footer.php'; ?>
