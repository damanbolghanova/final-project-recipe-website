<?php
require 'php/includes/header.php';
 ?>
<title>Recipes</title>
<link href="css/recipes.css" rel="stylesheet">
        <!--Recipes-->
        <h1 style="padding-top: 20px; text-align: center; font-size: 35px; font-family: Dancing Script;">What would you like to cook?</h1>
        <div class="red">
            <div class="block1">
                <div class="breakfast">
                    <img src="https://images.squarespace-cdn.com/content/v1/5280c154e4b05645e635cf76/1547771708977-7U2M3MYWWVWKXMR3MATF/ke17ZwdGBToddI8pDm48kDNmyC5FM8GiR4og3MgQa1x7gQa3H78H3Y0txjaiv_0fDoOvxcdMmMKkDsyUqMSsMWxHk725yiiHCCLfrh8O1z4YTzHvnKhyp6Da-NYroOW3ZGjoBKy3azqku80C789l0tb-hnCqoepq4X8c1traqO_JWRA3fEpTuA5K9wzrbyCcSebwWA2O1ajdB4h3_NxcMA/Sweet+Potato+Green+Curry+Bowl?format=1500w" width="240px" height="240px">
                </div>

                <div class="breakfast-text">
                    <h3 style="text-align: center;"><a href="breakfast.php" style="color: #FFA177;">Breakfast</a></h3>
                <p style="font-size: 28px; text-align: center; font-family: 'Dancing Script', cursive;">What nicer thing can you do for somebody than make them breakfast?</p>
                </div>

            </div>

            <div class="block2">
                <div class="lunch-text">
                    <h3 style="text-align: center; color: #FFA177;"><a href="lunch.php" style="color: #FFA177;">Lunch</a></h3>
                    <p style="font-size: 28px; text-align: center; font-family: 'Dancing Script', cursive;">Keep Calm and Take Lunch Breaks</p>
                </div>

                <div class="lunch">
                    <img src="https://images.squarespace-cdn.com/content/v1/5280c154e4b05645e635cf76/1524187406463-GVFCM2HHB28JVEM0FZTD/ke17ZwdGBToddI8pDm48kCHChmuivJZ1Va5ov3ZJeg17gQa3H78H3Y0txjaiv_0fDoOvxcdMmMKkDsyUqMSsMWxHk725yiiHCCLfrh8O1z4YTzHvnKhyp6Da-NYroOW3ZGjoBKy3azqku80C789l0ouw-8l5B_J38LMU7OZFvYcSGirBhY_3j1yQtntvGS73bypqQ-qjSV5umPUlGbQFAw/All+Recipes+from+This+Healthy+Table?format=1500w" width="260px" height="240px">
                </div>

            </div>

            <div class="block3">
                <div class="snack">
                    <img src="https://images.squarespace-cdn.com/content/v1/5280c154e4b05645e635cf76/1510623075171-YSO8RPB2QR8MG7393Z2T/ke17ZwdGBToddI8pDm48kDNmyC5FM8GiR4og3MgQa1x7gQa3H78H3Y0txjaiv_0fDoOvxcdMmMKkDsyUqMSsMWxHk725yiiHCCLfrh8O1z4YTzHvnKhyp6Da-NYroOW3ZGjoBKy3azqku80C789l0tb-hnCqoepq4X8c1traqO_JWRA3fEpTuA5K9wzrbyCcSebwWA2O1ajdB4h3_NxcMA/Blueberry+Basil+Coconut+Milk+Popsicles.+A+refreshing+dessert+%7C+This+Healthy+Table?format=1500w" width="240px" height="240px">
                </div>

                <div class="snack-text">
                    <h3 style="text-align: center; color: #FFA177;"><a href="snack.php" style="color: #FFA177;">Snack</a></h3>
                    <p style="font-size: 28px; text-align: center; font-family: 'Dancing Script', cursive;">That Feeling You Get In Your Stomach When You See <s>Him</s> Food</p>
                </div>
            </div>

            <div class="block4">
                <div class="diner-text">
                    <h3 style="text-align: center; color: #FFA177;"><a href="dinner.php" style="color: #FFA177;">Dinner</a></h3>
                    <p style="font-size: 28px; text-align: center; font-family: 'Dancing Script', cursive;">Dinner Time, Y'all</p>
                </div>

                <div class="dinner">
                    <img src="https://images.squarespace-cdn.com/content/v1/5280c154e4b05645e635cf76/1539571131905-NIWTM2YFZW2VFPP2EG93/ke17ZwdGBToddI8pDm48kD5zSyQvmWVSVepd6XN5Xrl7gQa3H78H3Y0txjaiv_0fDoOvxcdMmMKkDsyUqMSsMWxHk725yiiHCCLfrh8O1z4YTzHvnKhyp6Da-NYroOW3ZGjoBKy3azqku80C789l0oGwQPSn8VqSSM4mc7rOnog1Yt5KE5FZjfXkiSD8gka8cpvRYV1AoYBAa__gClmvgg/This+pumpkin+minestrone+soup+recipe+is+perfect+for+fall+-+it%E2%80%99s+hearty%2C+healthy%2C+and+packed+with+flavor.+%23pumpkin+%23soup+%23recipes?format=1500w" width="240px" height="240px">
                </div>
            </div>
        </div>

        <!--Footer-->
        <section id="footer">
            <div class="container">
                <div class="row">
                    <div class="col-md-4 footer-box">
                        <img src="logo.png" width="200" height="150">
                    </div>

                    <div class="col-md-4 footer-box">
                        <p><b>Contact Us</b></p>
                        <p><img src="https://static.tildacdn.com/tild6661-3261-4366-b737-613662323136/place-localizer.png" width="20" height="20"> Nur-Sultan city, Kazakhstan</p>
                        <p><img src="https://davidpou.net/wp-content/uploads/2017/08/Ic_phone_48px.png" width="20" height="20"> +7 707 316 08 09</p>
                        <p><img src="https://cdn2.iconfinder.com/data/icons/multimedia-outline-ve2/32/envelope_email_mail_message_letter-512.png" width="20" height="20"> damanbolghanova@gmail.com</p>
                    </div>

                    <div class="col-md-4 footer-box">
                        <p style="text-transform: uppercase;"><b>Subscribe Newsletter</b></p>
                        <input type="email" class="form-control" id="emailSub" required placeholder="Your Email">
                        <button type="button" class="btn btn-primary" onclick="subscribe()">Subscribe</button>
                    </div>
                </div>
            </div>
        </section>
<?php  require 'php/includes/footer.php'; ?>
