<?php
require 'php/includes/database.php';
$sql = "SELECT * FROM snacks_tb"; //RETRIEVE SMTH FROM DATABASE
$res = mysqli_query($conn, $sql); //execute the query
if ($res === false) {
  echo mysqli_error($conn);
}else{
  $recipes = mysqli_fetch_all($res, MYSQLI_ASSOC); //retrieves * from DATABASE
  //MYSQLI_ASSOC is for in what structure it will save the result: in this example
  // it will save it as a associative array take field as their key values
}
require 'php/includes/header.php';
?>
<title>Breakfast</title>
<link href="css/breakfast.css" rel="stylesheet">
        <!--Lunch-->
        <h1 style="padding-top: 20px;">I'm Sorry For What I Said When I Was Hungry</h1>
        <h1 style="padding-top: 20px;"><b>Snacks</b></h1>

        <div class="container">
          <?php foreach ($recipes as $recipe): ?>
            <div class="Item1">
              <img src="<?php echo $recipe['recipe_img']; ?>"><br>
              <h2><a href="recipeSnack.php?id=<?=$recipe['id'];?>"><?=$recipe['recipe_name'];?></a></h2>
            </div>
          <?php endforeach; ?>

          <a href="php/add/addSnacks.php">Add New Recipe</a>
        </div>
